from lecture import lectureCSV
from array import *
import argparse



def affichageProba(listeSituation,listePossibilite,listeDic,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    indiceSituation = 0
    indicePossibilite = 0
    while(indiceSituation < NOMBRE_SOMMET):
        print("=======================================================================")
        while(indicePossibilite < NOMBRE_SOMMET):
            print("Probabilite que dans la situation "+listeSituation[indiceSituation] + " on fasse "+listePossibilite[indicePossibilite] + " : "+listeDic[indiceSituation][listePossibilite[indicePossibilite]])
            indicePossibilite = indicePossibilite+1
        print("=======================================================================")
        indiceSituation = indiceSituation+1
        indicePossibilite = 0

def initialisationTableau(listeSituation,listePossibilite,listeDic,tableauProbabilite,tableauSortie,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    indiceSituation = 0
    indicePossibilite = 0
    while(indiceSituation < NOMBRE_SOMMET):
            while(indicePossibilite < NOMBRE_SOMMET):
                tableauProbabilite[indiceSituation][indicePossibilite]=listeDic[indiceSituation][listePossibilite[indicePossibilite]]
                indicePossibilite = indicePossibilite+1
            indiceSituation = indiceSituation+1
            indicePossibilite = 0
    indiceSituation = 0
    print()
    while(indiceSituation < NOMBRE_SOMMET):
        tableauSortie[0][indiceSituation] = input("Initialisation "+listeSituation[indiceSituation]+" : ")
        tableauSortie[0][indiceSituation] = float(tableauSortie[0][indiceSituation])
        indiceSituation = indiceSituation+1
    print()
    
    



def calcul(tableauProbabilite,tableauSortie,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    for indiceLigne in range(NOMBRE_NIVEAU):
        for indiceColonne in range(NOMBRE_SOMMET):
            nombreLigneDessous = 0
            for indiceParcoursProba in range(NOMBRE_SOMMET):
                nombreLigneDessous = nombreLigneDessous + float(tableauProbabilite[indiceParcoursProba][indiceColonne])*float(tableauSortie[indiceLigne][indiceParcoursProba])
                nombreLigneDessous = round(nombreLigneDessous,4)
            tableauSortie[indiceLigne+1][indiceColonne] = nombreLigneDessous

def affichageResulat(tableauSortie,listeSituation,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    print("",end=" ")
    for index in range(NOMBRE_SOMMET):
        print(listeSituation[index], end='           ')
    print()
    for r in range(NOMBRE_NIVEAU+1):
            print("Niveau "+str(r)+ " ------> ",end='')
            for c in range(NOMBRE_SOMMET):
                print("{:10.4f}".format(float(tableauSortie[r][c])), end=' ')
            print()


def main():
    parser = argparse.ArgumentParser(description="Propose une etude theorique de l'evolution d'une situation")
    parser.add_argument('cheminExercice', help="chemin de l'exercice")
    parser.add_argument('NombreNiveau', help="Nombre de niveau de l'exercice")
    args = parser.parse_args()
    CHEMIN_EXERCICE = args.cheminExercice
    NOMBRE_NIVEAU = int(args.NombreNiveau)
    listeDic = lectureCSV(CHEMIN_EXERCICE,'|')
    listePossibilite = []
    listeSituation = []
    tableauProbabilite = [[0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
    tableauSortie = []
    for index in range(NOMBRE_NIVEAU+2):
        tableauSortie.append([0,0,0,0,0])

    for champs in listeDic[0]:
        listePossibilite.append(champs)
    listePossibilite.remove("situation")
    if "possibilite" in listePossibilite:
        listePossibilite.remove("possibilite")
    NOMBRE_SOMMET = len(listePossibilite)
    for ligne in listeDic:
        listeSituation.append(ligne["situation"])
    affichageProba(listeSituation,listePossibilite,listeDic,NOMBRE_SOMMET,NOMBRE_NIVEAU)
    initialisationTableau(listeSituation,listePossibilite,listeDic,tableauProbabilite,tableauSortie,NOMBRE_SOMMET,NOMBRE_NIVEAU)
    calcul(tableauProbabilite,tableauSortie,NOMBRE_SOMMET,NOMBRE_NIVEAU)
    affichageResulat(tableauSortie,listeSituation,NOMBRE_SOMMET,NOMBRE_NIVEAU)
    

if __name__ == "__main__":
    main()
