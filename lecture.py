import csv

def lectureCSV(chemin,delimiteur):
    dictionnaire = []
    with open(chemin, 'r') as entree:
        csvreader = csv.DictReader(entree, delimiter=delimiteur)
        indice = 0
        for contenu in csvreader:
            dictionnaire.append(contenu)
    
        nouvelleListe = []
        for sousDic in dictionnaire:
            if 'possibilite' in dictionnaire:
                del sousDic["possibilite"]
        return dictionnaire