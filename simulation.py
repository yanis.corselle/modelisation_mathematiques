from lecture import lectureCSV
from array import *
import argparse
import random



def affichageProba(listeSituation,listePossibilite,listeDic,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    indiceSituation = 0
    indicePossibilite = 0
    while(indiceSituation < NOMBRE_SOMMET):
        print("=======================================================================")
        while(indicePossibilite < NOMBRE_SOMMET):
            print("Probabilite que dans la situation "+listeSituation[indiceSituation] + " on fasse "+listePossibilite[indicePossibilite] + " : "+listeDic[indiceSituation][listePossibilite[indicePossibilite]])
            indicePossibilite = indicePossibilite+1
        print("=======================================================================")
        indiceSituation = indiceSituation+1
        indicePossibilite = 0

def initialisationTableau(listeSituation,listePossibilite,listeDic,tableauProbabilite,tableauSortie,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    indiceSituation = 0
    indicePossibilite = 0
    while(indiceSituation < NOMBRE_SOMMET):
            while(indicePossibilite < NOMBRE_SOMMET):
                tableauProbabilite[indiceSituation][indicePossibilite]=listeDic[indiceSituation][listePossibilite[indicePossibilite]]
                indicePossibilite = indicePossibilite+1
            indiceSituation = indiceSituation+1
            indicePossibilite = 0
    indiceSituation = 0


def initialisationObjet(listeSituation,tableauSortie,NOMBRE_SOMMET):
    print()
    indiceSituation = 0
    while(indiceSituation < NOMBRE_SOMMET):
        tableauSortie[0][indiceSituation] = input("Initialisation "+listeSituation[indiceSituation]+" : ")
        indiceSituation = indiceSituation+1
    print()
    

def simulation(tableauSortie,tableauProbabilite,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    for indiceLigne in range(NOMBRE_NIVEAU+1):
        for indiceColonne in range(NOMBRE_SOMMET):
            NbObjetDansCase = int(tableauSortie[indiceLigne][indiceColonne])
            if NbObjetDansCase >= 1:
                for NbObjetTraites in range(NbObjetDansCase):
                    nombreAleatoire = random.uniform(0.0,1.0)
                    MINI = 0
                    MAX = float(tableauProbabilite[indiceColonne][0])
                    continuer = True
                    indiceRetour = 0
                    while(continuer == True):
                        if (nombreAleatoire >= MINI and nombreAleatoire <= MAX):
                            continuer = False
                        else:
                            indiceRetour = indiceRetour + 1
                            MINI = MAX
                            if indiceRetour == NOMBRE_SOMMET-1:
                                MAX = 100
                            else:
                                MAX = MAX + float(tableauProbabilite[indiceColonne][indiceRetour])
                    tableauSortie[indiceLigne+1][indiceRetour]+=1


          

    

def affichageResulat(tableauSortie,listeSituation,NOMBRE_SOMMET,NOMBRE_NIVEAU):
    print("      ",end="   ")
    for index in range(NOMBRE_SOMMET):
        print(listeSituation[index], end='           ')
    print()
    for r in range(NOMBRE_NIVEAU+1):
            print("Niveau "+str(r)+ " ------> ",end='')
            for c in range(NOMBRE_SOMMET):
                print("{:10.4f}".format(float(tableauSortie[r][c])), end=' ')
            print()


def main():
    parser = argparse.ArgumentParser(description="Propose une etude theorique de l'evolution d'une situation")
    parser.add_argument('cheminExercice', help="chemin de l'exercice")
    parser.add_argument('NombreNiveau', help="Nombre de niveau de l'exercice")
    parser.add_argument('--nb', help="Indique le nombre de fois à realiser l'experience.")
    args = parser.parse_args()
    CHEMIN_EXERCICE = args.cheminExercice
    NOMBRE_NIVEAU = int(args.NombreNiveau)
    NB = 1
    if args.nb:
        NB = int(args.nb)
    listeDic = lectureCSV(CHEMIN_EXERCICE,'|')
    listePossibilite = []
    listeSituation = []
    tableauProbabilite = [[0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
    tableauSortie = []
    for index in range(NOMBRE_NIVEAU+2):
        tableauSortie.append([0,0,0,0,0])

    for champs in listeDic[0]:
        listePossibilite.append(champs)
    listePossibilite.remove("situation")
    if "possibilite" in listePossibilite:
        listePossibilite.remove("possibilite")
    NOMBRE_SOMMET = len(listePossibilite)
    for ligne in listeDic:
        listeSituation.append(ligne["situation"])
    tableauBilan = []
    for index in range(NOMBRE_NIVEAU+2):
            tableauBilan.append([0,0,0,0,0])
    affichageProba(listeSituation,listePossibilite,listeDic,NOMBRE_SOMMET,NOMBRE_NIVEAU)
    tableauSortie = []
    for index in range(NOMBRE_NIVEAU+2):
            tableauSortie.append([0,0,0,0,0])
    initialisationObjet(listeSituation,tableauSortie,NOMBRE_SOMMET)
    nbObject = 0
    for index in range(NOMBRE_SOMMET):
        nbObject = nbObject + int(tableauSortie[0][index])
        print(nbObject)
    for index in range(NB):
        for index in range(NOMBRE_SOMMET+1):
            tableauProbabilite.append([0,0,0,0,0,0,0,0,0,0])
        for index in range(1,NOMBRE_NIVEAU+2):
            tableauSortie[index] = [0,0,0,0,0]
        initialisationTableau(listeSituation,listePossibilite,listeDic,tableauProbabilite,tableauSortie,NOMBRE_SOMMET,NOMBRE_NIVEAU)
        simulation(tableauSortie,tableauProbabilite,NOMBRE_SOMMET,NOMBRE_NIVEAU)
        for ligne in range(NOMBRE_NIVEAU+1):
            for c in range(NOMBRE_SOMMET):
                tableauBilan[ligne][c] += float(tableauSortie[ligne][c])
                
    for ligne in range(NOMBRE_NIVEAU+1):
            for c in range(NOMBRE_SOMMET):
                tableauBilan[ligne][c] = (float(tableauBilan[ligne][c])/float(NB))*100/nbObject


    affichageResulat(tableauBilan,listeSituation,NOMBRE_SOMMET,NOMBRE_NIVEAU)
    

if __name__ == "__main__":
    main()
